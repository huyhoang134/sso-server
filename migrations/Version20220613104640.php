<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220613104640 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE security_tokens ADD CONSTRAINT FK_9CB5E79B2AB34170 FOREIGN KEY (wde_declar_id) REFERENCES wdeclar (wdeDeclar)');
        $this->addSql('ALTER TABLE security_tokens ADD CONSTRAINT FK_9CB5E79B7987212D FOREIGN KEY (app_id) REFERENCES security_authorized_apps (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE security_tokens DROP FOREIGN KEY FK_9CB5E79B2AB34170');
        $this->addSql('ALTER TABLE security_tokens DROP FOREIGN KEY FK_9CB5E79B7987212D');
    }
}
