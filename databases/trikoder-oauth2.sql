-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:8889
-- Thời gian đã tạo: Th6 22, 2022 lúc 09:57 AM
-- Phiên bản máy phục vụ: 5.7.34
-- Phiên bản PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `trikoder-oauth2`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth2_access_token`
--

CREATE TABLE `oauth2_access_token` (
  `identifier` char(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `user_identifier` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:oauth2_scope)',
  `revoked` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth2_authorization_code`
--

CREATE TABLE `oauth2_authorization_code` (
  `identifier` char(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `user_identifier` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:oauth2_scope)',
  `revoked` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth2_client`
--

CREATE TABLE `oauth2_client` (
  `identifier` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect_uris` text COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:oauth2_redirect_uri)',
  `grants` text COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:oauth2_grant)',
  `scopes` text COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:oauth2_scope)',
  `active` tinyint(1) NOT NULL,
  `allow_plain_text_pkce` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth2_client`
--

INSERT INTO `oauth2_client` (`identifier`, `secret`, `redirect_uris`, `grants`, `scopes`, `active`, `allow_plain_text_pkce`) VALUES
('b5f5484cceff1f7a9c182721799a914a', '30882e14c19a0f87a70b157b58c684c0fa19609d56384c520da1660f005d0aa44635acf5e3b2d935208a5f6448a5cba8a101a4a896d5bb6113ce9acfa7043313', 'https://delef.local/', 'client_credentials authorization_code', 'read', 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth2_refresh_token`
--

CREATE TABLE `oauth2_refresh_token` (
  `identifier` char(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` char(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `revoked` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `security_authorized_apps`
--

CREATE TABLE `security_authorized_apps` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `return_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_allowed_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `security_authorized_apps`
--

INSERT INTO `security_authorized_apps` (`id`, `name`, `type`, `return_url`, `key`, `secret`, `is_allowed_admin`) VALUES
(1, 'test Client delef', 'web', 'https://delef.local/', 'b5f5484cceff1f7a9c182721799a914a', '30882e14c19a0f87a70b157b58c684c0fa19609d56384c520da1660f005d0aa44635acf5e3b2d935208a5f6448a5cba8a101a4a896d5bb6113ce9acfa7043313', 1),
(2, 'test Client delef2', 'web', 'https://delef2.local/', 'c5f5484cceff1f7a9c182721799a914a', '40882e14c19a0f87a70b157b58c684c0fa19609d56384c520da1660f005d0aa44635acf5e3b2d935208a5f6448a5cba8a101a4a896d5bb6113ce9acfa7043313', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `security_tokens`
--

CREATE TABLE `security_tokens` (
  `id` int(11) NOT NULL,
  `wde_declar_id` int(11) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `hash` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `last_used_at` datetime NOT NULL,
  `expires_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------


--
-- Đang đổ dữ liệu cho bảng `wdeclar`
--

--
-- Chỉ mục cho bảng `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Chỉ mục cho bảng `oauth2_access_token`
--
ALTER TABLE `oauth2_access_token`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `IDX_454D9673C7440455` (`client`);

--
-- Chỉ mục cho bảng `oauth2_authorization_code`
--
ALTER TABLE `oauth2_authorization_code`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `IDX_509FEF5FC7440455` (`client`);

--
-- Chỉ mục cho bảng `oauth2_client`
--
ALTER TABLE `oauth2_client`
  ADD PRIMARY KEY (`identifier`);

--
-- Chỉ mục cho bảng `oauth2_refresh_token`
--
ALTER TABLE `oauth2_refresh_token`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `IDX_4DD90732B6A2DD68` (`access_token`);

--
-- Chỉ mục cho bảng `security_authorized_apps`
--
ALTER TABLE `security_authorized_apps`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_D3613E335E237E06` (`name`),
  ADD UNIQUE KEY `UNIQ_D3613E334E645A7E` (`key`),
  ADD UNIQUE KEY `UNIQ_D3613E335CA2E8E5` (`secret`);

--
-- Chỉ mục cho bảng `security_tokens`
--
ALTER TABLE `security_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9CB5E79B2AB34170` (`wde_declar_id`),
  ADD KEY `IDX_9CB5E79B7987212D` (`app_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);



--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `security_authorized_apps`
--
ALTER TABLE `security_authorized_apps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `security_tokens`
--
ALTER TABLE `security_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `wdeclar`
--
ALTER TABLE `wdeclar`
  MODIFY `wde_declar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17729;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `oauth2_access_token`
--
ALTER TABLE `oauth2_access_token`
  ADD CONSTRAINT `FK_454D9673C7440455` FOREIGN KEY (`client`) REFERENCES `oauth2_client` (`identifier`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `oauth2_authorization_code`
--
ALTER TABLE `oauth2_authorization_code`
  ADD CONSTRAINT `FK_509FEF5FC7440455` FOREIGN KEY (`client`) REFERENCES `oauth2_client` (`identifier`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `oauth2_refresh_token`
--
ALTER TABLE `oauth2_refresh_token`
  ADD CONSTRAINT `FK_4DD90732B6A2DD68` FOREIGN KEY (`access_token`) REFERENCES `oauth2_access_token` (`identifier`) ON DELETE SET NULL;

--
-- Các ràng buộc cho bảng `security_tokens`
--
ALTER TABLE `security_tokens`
  ADD CONSTRAINT `FK_9CB5E79B2AB34170` FOREIGN KEY (`wde_declar_id`) REFERENCES `wdeclar` (`wde_declar`),
  ADD CONSTRAINT `FK_9CB5E79B7987212D` FOREIGN KEY (`app_id`) REFERENCES `security_authorized_apps` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;





REATE ALGORITHM = MERGE VIEW `SSO` AS 
    SELECT
    CAST(
        `cfc_delef`.`wcocon`.`wco_dossier` AS CHAR CHARSET utf8mb4
    ) AS `sso_login`,
    `cfc_delef`.`wcocon`.`wco_password` AS `sso_password`,
    'wcocon' AS `sso_table`,
    `cfc_delef`.`wcocon`.`wco_dossier` AS `sso_id_reccord`,
    MD5(
        CONCAT(
            CAST(
                `cfc_delef`.`wcocon`.`wco_dossier` AS CHAR CHARSET utf8mb4
            ),
            `cfc_delef`.`wcocon`.`wco_password`
        )
    ) AS `sso_token`
FROM
    `cfc_delef`.`wcocon`
UNION ALL
SELECT
    CAST(
        `cfc_delef`.`wdeclar`.`wde_dossier` AS CHAR CHARSET utf8mb4
    ) AS `sso_login`,
    `cfc_delef`.`wcocon`.`wco_password` AS `sso_password`,
    'wdeclar' AS `sso_table`,
    `cfc_delef`.`wdeclar`.`wde_declar` AS `sso_id_reccord`,
    MD5(
        CONCAT(
            CAST(
                `cfc_delef`.`wdeclar`.`wde_dossier` AS CHAR CHARSET utf8mb4
            ),
            `cfc_delef`.`wcocon`.`wco_password`
        )
    ) AS `sso_token`
FROM
    (
        `cfc_delef`.`wcocon`
    JOIN `cfc_delef`.`wdeclar` ON
        (
            (
                `cfc_delef`.`wdeclar`.`wde_dossier` = `cfc_delef`.`wcocon`.`wco_dossier`
            )
        )
    )
UNION ALL
SELECT
    CAST(
        `cfc_delef`.`wdeclar`.`wde_dossier` AS CHAR CHARSET utf8mb4
    ) AS `sso_login`,
    `cfc_delef`.`wdeclar`.`wde_password` AS `sso_password`,
    'wdeclar' AS `sso_table`,
    `cfc_delef`.`wdeclar`.`wde_declar` AS `sso_id_reccord`,
    MD5(
        CONCAT(
            CAST(
                `cfc_delef`.`wdeclar`.`wde_dossier` AS CHAR CHARSET utf8mb4
            ),
            `cfc_delef`.`wdeclar`.`wde_password`
        )
    ) AS `sso_token`
FROM
    `cfc_delef`.`wdeclar`
