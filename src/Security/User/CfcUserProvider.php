<?php 
namespace App\Security\User;

use App\Entity\Wcontrat;
use App\Entity\Sso;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

//use ResultSetMapping as entity don't have Primary key (can not use doctrine repository)
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Entity\Token;
use App\Entity\AuthorizedApp;
use Doctrine\Common\Collections\ArrayCollection;

class CfcUserProvider implements UserProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->em=$entityManager;
    }
    
    
    public function loadUserByUsername($username)
    {
        return $this->fetchUser($username);
    }

    public function loadUserByUsernamePassword($username, $password)
    {

        $em=$this->entityManager->createQuery()->getEntityManager();
        
        $rsm = new ResultSetMapping();
        //$rsm->addScalarResult('sso_login', 'sso_login');
        //$rsm->addScalarResult('sso_password', 'sso_password');
        $rsm->addScalarResult('sso_table', 'sso_table');
        $rsm->addScalarResult('sso_id_reccord', 'sso_id_reccord');
        $rsm->addScalarResult('sso_token', 'sso_token');
        
        $query = $em->createNativeQuery("SELECT sso_login, sso_password, sso_table, sso_id_reccord, sso_token    FROM `SSO` where sso_login='".$username."' and sso_password='".$password."'", $rsm);
        $results = $query->getResult();
        return $results;

        //return $this->fetchUser($username);
    }
    
    public function loadUserByUserToken($userToken)
    {
    
        $em=$this->entityManager->createQuery()->getEntityManager();
    
        $rsm = new ResultSetMapping();
        //$rsm->addScalarResult('sso_login', 'sso_login');
        //$rsm->addScalarResult('sso_password', 'sso_password');
        $rsm->addScalarResult('sso_table', 'sso_table');
        $rsm->addScalarResult('sso_id_reccord', 'sso_id_reccord');
    
    
        $query = $em->createNativeQuery("SELECT sso_login, sso_password, sso_table, sso_id_reccord    FROM `SSO` where sso_token='".$userToken."'", $rsm);
        $results = $query->getResult();
        return $results;
    
        //return $this->fetchUser($username);
    }
    
    public function getMappedTokens($userToken, $app)
    {

       $accessToken= $this->entityManager->getRepository(Token::class)->findOneBy(['userToken'=>$userToken, 'app'=>$app->getId(), 'type' => Token::TOKEN_TYPE_ACCESS]);
   
    
    if ($accessToken !== null && $accessToken->getExpiresAt()->getTimestamp() < time()) {
        $this->em->remove($accessToken);
        $this->em->flush();
    }
    
    $app = $this->em->getRepository(AuthorizedApp::class)->find($app->getId());

    $tokens = $this->em->getRepository(Token::class)->findBy(['app' => $app, 'userToken' => $userToken]);
    

    if (count($tokens) < 3) {
        $existingTypes = [];
        foreach ($tokens as $token)
        {
            $existingTypes[] = $token->getType();
        }
    
        $newTokens = new ArrayCollection();
        if (!in_array(Token::TOKEN_TYPE_ACCESS, $existingTypes)) {
            $newTokens->add($this->em->getRepository(Token::class)->createAccessToken($userToken)->setApp($app));
        }
        //            if (!in_array(Token::TOKEN_TYPE_REFRESH, $existingTypes)) {
        //                $newTokens->add($this->em->getRepository(Token::class)->createRefreshToken($user)->setApp($app));
        //            }
        //            if (!in_array(Token::TOKEN_TYPE_VERIFIER, $existingTypes)) {
        //                $newTokens->add($this->em->getRepository(Token::class)->createVerifierToken($user)->setApp($app));
        //            }
    
        foreach ($newTokens as $token)
        {
            $this->em->persist($token);
            $this->em->flush();
        }
        }
    
        $tokenHash=[];
        $tokens = $this->em->getRepository(Token::class)->findBy(['app' => $app, 'userToken' => $userToken]);
        foreach ($tokens as $token)
        {
            $token->setExpiresAt((new \DateTime())->add(new \DateInterval("P6M")));
            $token->setLastUsedAt(new \DateTime());
            $this->em->persist($token);
            $this->em->flush();
            $tokenHash[$token->type]['hash']=$token->hash;
        }

        return $tokenHash;
        
    }
    
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof Wdeclar) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        $username = $user->getUsername();

        return $this->fetchUser($username);
    }

    public function supportsClass($class)
    {
        return Wcontrat::class === $class;
    }

    private function fetchUser($username)
    {
       return $this->entityManager->getRepository(Wcontrat::class)->findOneBy(['wctDossier ' => $username]);
    }
}

