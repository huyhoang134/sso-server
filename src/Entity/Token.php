<?php

namespace App\Entity;

use App\Entity\AuthorizedApp;
use App\Repository\TokenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="security_tokens")
 * @ORM\Entity(repositoryClass=TokenRepository::class)
 */
class Token
{
    public const TOKEN_TYPE_ACCESS = 'access_token';
    public const TOKEN_TYPE_REFRESH = 'refresh_token';
    public const TOKEN_TYPE_VERIFIER = 'verifier_token';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    public $hash;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastUsedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expiresAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userToken;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AuthorizedApp", inversedBy="tokens")
     */
    private $app;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     * @return Token
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Token
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Token
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getLastUsedAt()
    {
        return $this->lastUsedAt;
    }

    /**
     * @param mixed $lastUsedAt
     * @return Token
     */
    public function setLastUsedAt($lastUsedAt)
    {
        $this->lastUsedAt = $lastUsedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @param mixed $expiresAt
     * @return Token
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserToken()
    {
        return $this->userToken;
    }

    /**
     * @param mixed $userTocken
     * @return Token
     */
    public function setUserToken($user)
    {
        $this->userToken = $user;
        return $this;
    }

    /**
     * @return null|AuthorizedApp
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @param mixed $app
     * @return Token
     */
    public function setApp($app)
    {
        $this->app = $app;
        return $this;
    }

    public function __toString()
    {
        return $this->getHash();
    }
}
