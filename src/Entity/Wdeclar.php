<?php

namespace App\Entity;

use App\Entity\AuthorizedApp;
use App\Entity\Token;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Wdeclar
 *
 * @ORM\Table(name="wdeclar", indexes={@ORM\Index(name="uk_wdeclar", columns={"wde_contrat", "wde_annee"}), @ORM\Index(name="fk_wdeclar_cocon", columns={"wde_dossier"})})
 * @ORM\Entity
 */
class Wdeclar implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="wde_declar", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $wdeDeclar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_dossier", type="integer", nullable=true)
     */
    private $wdeDossier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_password", type="string", length=8, nullable=true)
     */
    private $wdePassword;

    /**
     * @var string
     *
     * @ORM\Column(name="wde_typcont", type="string", length=50, nullable=false)
     */
    private $wdeTypcont;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="wde_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $wdeStamp = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="wde_synchro", type="datetime", nullable=true)
     */
    private $wdeSynchro;

    /**
     * @var int
     *
     * @ORM\Column(name="wde_contrat", type="integer", nullable=false)
     */
    private $wdeContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="wde_annee", type="string", length=12, nullable=false)
     */
    private $wdeAnnee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_numperiod", type="string", length=255, nullable=true)
     */
    private $wdeNumperiod;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_eleves", type="integer", nullable=true)
     */
    private $wdeEleves;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_tranche", type="string", length=3, nullable=true)
     */
    private $wdeTranche;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_copieurs", type="integer", nullable=true)
     */
    private $wdeCopieurs;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_profs", type="integer", nullable=true)
     */
    private $wdeProfs;

    /**
     * @var string
     *
     * @ORM\Column(name="wde_etat_declar", type="string", length=1, nullable=false)
     */
    private $wdeEtatDeclar;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="wde_date_validation", type="datetime", nullable=true)
     */
    private $wdeDateValidation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_email_sent", type="smallint", nullable=true)
     */
    private $wdeEmailSent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_nom_resp", type="string", length=50, nullable=true)
     */
    private $wdeNomResp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_tit_resp", type="string", length=35, nullable=true)
     */
    private $wdeTitResp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_fct_resp", type="string", length=50, nullable=true)
     */
    private $wdeFctResp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_mel_resp", type="string", length=90, nullable=true)
     */
    private $wdeMelResp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_effectif", type="integer", nullable=true)
     */
    private $wdeEffectif;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche01", type="integer", nullable=true)
     */
    private $wdeTranche01;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche02", type="integer", nullable=true)
     */
    private $wdeTranche02;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche03", type="integer", nullable=true)
     */
    private $wdeTranche03;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche04", type="integer", nullable=true)
     */
    private $wdeTranche04;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche05", type="integer", nullable=true)
     */
    private $wdeTranche05;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche06", type="integer", nullable=true)
     */
    private $wdeTranche06;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche07", type="integer", nullable=true)
     */
    private $wdeTranche07;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_ct_nom_sec_gen_question", type="integer", nullable=true)
     */
    private $wdeIdCtNomSecGenQuestion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_ct_nom_sec_gen", type="integer", nullable=true)
     */
    private $wdeIdCtNomSecGen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_nom_sec_gen", type="string", length=50, nullable=true)
     */
    private $wdeNomSecGen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_tit_sec_gen", type="string", length=35, nullable=true)
     */
    private $wdeTitSecGen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_fct_sec_gen", type="string", length=50, nullable=true)
     */
    private $wdeFctSecGen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_mel_sec_gen", type="string", length=90, nullable=true)
     */
    private $wdeMelSecGen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_intitule_decl", type="string", length=255, nullable=true)
     */
    private $wdeIntituleDecl;

    /**
     * @var string
     *
     * @ORM\Column(name="wde_annee_decl", type="string", length=9, nullable=false)
     */
    private $wdeAnneeDecl;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche08", type="integer", nullable=true)
     */
    private $wdeTranche08;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche09", type="integer", nullable=true)
     */
    private $wdeTranche09;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche10", type="integer", nullable=true)
     */
    private $wdeTranche10;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche11", type="integer", nullable=true)
     */
    private $wdeTranche11;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche12", type="integer", nullable=true)
     */
    private $wdeTranche12;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche13", type="integer", nullable=true)
     */
    private $wdeTranche13;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche14", type="integer", nullable=true)
     */
    private $wdeTranche14;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche15", type="integer", nullable=true)
     */
    private $wdeTranche15;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche16", type="integer", nullable=true)
     */
    private $wdeTranche16;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche17", type="integer", nullable=true)
     */
    private $wdeTranche17;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche18", type="integer", nullable=true)
     */
    private $wdeTranche18;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche19", type="integer", nullable=true)
     */
    private $wdeTranche19;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche20", type="integer", nullable=true)
     */
    private $wdeTranche20;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche21", type="integer", nullable=true)
     */
    private $wdeTranche21;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_tranche22", type="integer", nullable=true)
     */
    private $wdeTranche22;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_ct_resp_question", type="integer", nullable=true)
     */
    private $wdeIdCtRespQuestion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_ct_resp", type="integer", nullable=true)
     */
    private $wdeIdCtResp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_tel_resp", type="string", length=26, nullable=true)
     */
    private $wdeTelResp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_fax_resp", type="string", length=26, nullable=true)
     */
    private $wdeFaxResp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_effectif2", type="integer", nullable=true)
     */
    private $wdeEffectif2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_effectif3", type="integer", nullable=true)
     */
    private $wdeEffectif3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_effectif4", type="integer", nullable=true)
     */
    private $wdeEffectif4;

    /**
     * @var int
     *
     * @ORM\Column(name="wde_is_chorus_pro", type="smallint", nullable=false)
     */
    private $wdeIsChorusPro;

    /**
     * @var string
     *
     * @ORM\Column(name="wde_siret", type="string", length=14, nullable=false)
     */
    private $wdeSiret;

    /**
     * @var string
     *
     * @ORM\Column(name="wde_service_code", type="string", length=100, nullable=false)
     */
    private $wdeServiceCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_is_order_number", type="smallint", nullable=true)
     */
    private $wdeIsOrderNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="wde_order_number", type="string", length=50, nullable=false)
     */
    private $wdeOrderNumber;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_ct_fact_question", type="integer", nullable=true)
     */
    private $wdeIdCtFactQuestion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_ct_fact", type="integer", nullable=true)
     */
    private $wdeIdCtFact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_nom_fact", type="string", length=50, nullable=true)
     */
    private $wdeNomFact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_tit_fact", type="string", length=35, nullable=true)
     */
    private $wdeTitFact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_fct_fact", type="string", length=50, nullable=true)
     */
    private $wdeFctFact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_raisoc1_fact", type="string", length=50, nullable=true)
     */
    private $wdeRaisoc1Fact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_adresse1_fact", type="string", length=50, nullable=true)
     */
    private $wdeAdresse1Fact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_adresse2_fact", type="string", length=50, nullable=true)
     */
    private $wdeAdresse2Fact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_adresse3_fact", type="string", length=50, nullable=true)
     */
    private $wdeAdresse3Fact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_copos_fact", type="string", length=16, nullable=true)
     */
    private $wdeCoposFact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_ville_fact", type="string", length=42, nullable=true)
     */
    private $wdeVilleFact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_tel_fact", type="string", length=26, nullable=true)
     */
    private $wdeTelFact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wde_mail_fact", type="string", length=80, nullable=true)
     */
    private $wdeMailFact;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_adr_fact_question", type="integer", nullable=true)
     */
    private $wdeIdAdrFactQuestion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="wde_id_adr_fact", type="integer", nullable=true)
     */
    private $wdeIdAdrFact;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Token", mappedBy="user")
     */
    private $tokens;

    /**
     * @var bool
     */
    private $instanceForAuthenticator;

    public function __construct()
    {
        $this->instanceForAuthenticator = false;
        $this->tokens                   = new ArrayCollection();
    }

    public function getWdeDeclar(): ?int
    {
        return $this->wdeDeclar;
    }

    public function getWdeDossier(): ?int
    {
        return $this->wdeDossier;
    }

    public function setWdeDossier(?int $wdeDossier): self
    {
        $this->wdeDossier = $wdeDossier;

        return $this;
    }

    public function getWdePassword(): ?string
    {
        return $this->wdePassword;
    }

    public function setWdePassword(?string $wdePassword): self
    {
        $this->wdePassword = $wdePassword;

        return $this;
    }

    public function getWdeTypcont(): ?string
    {
        return $this->wdeTypcont;
    }

    public function setWdeTypcont(string $wdeTypcont): self
    {
        $this->wdeTypcont = $wdeTypcont;

        return $this;
    }

    public function getWdeStamp(): ?\DateTimeInterface
    {
        return $this->wdeStamp;
    }

    public function setWdeStamp(\DateTimeInterface $wdeStamp): self
    {
        $this->wdeStamp = $wdeStamp;

        return $this;
    }

    public function getWdeSynchro(): ?\DateTimeInterface
    {
        return $this->wdeSynchro;
    }

    public function setWdeSynchro(?\DateTimeInterface $wdeSynchro): self
    {
        $this->wdeSynchro = $wdeSynchro;

        return $this;
    }

    public function getWdeContrat(): ?int
    {
        return $this->wdeContrat;
    }

    public function setWdeContrat(int $wdeContrat): self
    {
        $this->wdeContrat = $wdeContrat;

        return $this;
    }

    public function getWdeAnnee(): ?string
    {
        return $this->wdeAnnee;
    }

    public function setWdeAnnee(string $wdeAnnee): self
    {
        $this->wdeAnnee = $wdeAnnee;

        return $this;
    }

    public function getWdeNumperiod(): ?string
    {
        return $this->wdeNumperiod;
    }

    public function setWdeNumperiod(?string $wdeNumperiod): self
    {
        $this->wdeNumperiod = $wdeNumperiod;

        return $this;
    }

    public function getWdeEleves(): ?int
    {
        return $this->wdeEleves;
    }

    public function setWdeEleves(?int $wdeEleves): self
    {
        $this->wdeEleves = $wdeEleves;

        return $this;
    }

    public function getWdeTranche(): ?string
    {
        return $this->wdeTranche;
    }

    public function setWdeTranche(?string $wdeTranche): self
    {
        $this->wdeTranche = $wdeTranche;

        return $this;
    }

    public function getWdeCopieurs(): ?int
    {
        return $this->wdeCopieurs;
    }

    public function setWdeCopieurs(?int $wdeCopieurs): self
    {
        $this->wdeCopieurs = $wdeCopieurs;

        return $this;
    }

    public function getWdeProfs(): ?int
    {
        return $this->wdeProfs;
    }

    public function setWdeProfs(?int $wdeProfs): self
    {
        $this->wdeProfs = $wdeProfs;

        return $this;
    }

    public function getWdeEtatDeclar(): ?string
    {
        return $this->wdeEtatDeclar;
    }

    public function setWdeEtatDeclar(string $wdeEtatDeclar): self
    {
        $this->wdeEtatDeclar = $wdeEtatDeclar;

        return $this;
    }

    public function getWdeDateValidation(): ?\DateTimeInterface
    {
        return $this->wdeDateValidation;
    }

    public function setWdeDateValidation(?\DateTimeInterface $wdeDateValidation): self
    {
        $this->wdeDateValidation = $wdeDateValidation;

        return $this;
    }

    public function getWdeEmailSent(): ?int
    {
        return $this->wdeEmailSent;
    }

    public function setWdeEmailSent(?int $wdeEmailSent): self
    {
        $this->wdeEmailSent = $wdeEmailSent;

        return $this;
    }

    public function getWdeNomResp(): ?string
    {
        return $this->wdeNomResp;
    }

    public function setWdeNomResp(?string $wdeNomResp): self
    {
        $this->wdeNomResp = $wdeNomResp;

        return $this;
    }

    public function getWdeTitResp(): ?string
    {
        return $this->wdeTitResp;
    }

    public function setWdeTitResp(?string $wdeTitResp): self
    {
        $this->wdeTitResp = $wdeTitResp;

        return $this;
    }

    public function getWdeFctResp(): ?string
    {
        return $this->wdeFctResp;
    }

    public function setWdeFctResp(?string $wdeFctResp): self
    {
        $this->wdeFctResp = $wdeFctResp;

        return $this;
    }

    public function getWdeMelResp(): ?string
    {
        return $this->wdeMelResp;
    }

    public function setWdeMelResp(?string $wdeMelResp): self
    {
        $this->wdeMelResp = $wdeMelResp;

        return $this;
    }

    public function getWdeEffectif(): ?int
    {
        return $this->wdeEffectif;
    }

    public function setWdeEffectif(?int $wdeEffectif): self
    {
        $this->wdeEffectif = $wdeEffectif;

        return $this;
    }

    public function getWdeTranche01(): ?int
    {
        return $this->wdeTranche01;
    }

    public function setWdeTranche01(?int $wdeTranche01): self
    {
        $this->wdeTranche01 = $wdeTranche01;

        return $this;
    }

    public function getWdeTranche02(): ?int
    {
        return $this->wdeTranche02;
    }

    public function setWdeTranche02(?int $wdeTranche02): self
    {
        $this->wdeTranche02 = $wdeTranche02;

        return $this;
    }

    public function getWdeTranche03(): ?int
    {
        return $this->wdeTranche03;
    }

    public function setWdeTranche03(?int $wdeTranche03): self
    {
        $this->wdeTranche03 = $wdeTranche03;

        return $this;
    }

    public function getWdeTranche04(): ?int
    {
        return $this->wdeTranche04;
    }

    public function setWdeTranche04(?int $wdeTranche04): self
    {
        $this->wdeTranche04 = $wdeTranche04;

        return $this;
    }

    public function getWdeTranche05(): ?int
    {
        return $this->wdeTranche05;
    }

    public function setWdeTranche05(?int $wdeTranche05): self
    {
        $this->wdeTranche05 = $wdeTranche05;

        return $this;
    }

    public function getWdeTranche06(): ?int
    {
        return $this->wdeTranche06;
    }

    public function setWdeTranche06(?int $wdeTranche06): self
    {
        $this->wdeTranche06 = $wdeTranche06;

        return $this;
    }

    public function getWdeTranche07(): ?int
    {
        return $this->wdeTranche07;
    }

    public function setWdeTranche07(?int $wdeTranche07): self
    {
        $this->wdeTranche07 = $wdeTranche07;

        return $this;
    }

    public function getWdeIdCtNomSecGenQuestion(): ?int
    {
        return $this->wdeIdCtNomSecGenQuestion;
    }

    public function setWdeIdCtNomSecGenQuestion(?int $wdeIdCtNomSecGenQuestion): self
    {
        $this->wdeIdCtNomSecGenQuestion = $wdeIdCtNomSecGenQuestion;

        return $this;
    }

    public function getWdeIdCtNomSecGen(): ?int
    {
        return $this->wdeIdCtNomSecGen;
    }

    public function setWdeIdCtNomSecGen(?int $wdeIdCtNomSecGen): self
    {
        $this->wdeIdCtNomSecGen = $wdeIdCtNomSecGen;

        return $this;
    }

    public function getWdeNomSecGen(): ?string
    {
        return $this->wdeNomSecGen;
    }

    public function setWdeNomSecGen(?string $wdeNomSecGen): self
    {
        $this->wdeNomSecGen = $wdeNomSecGen;

        return $this;
    }

    public function getWdeTitSecGen(): ?string
    {
        return $this->wdeTitSecGen;
    }

    public function setWdeTitSecGen(?string $wdeTitSecGen): self
    {
        $this->wdeTitSecGen = $wdeTitSecGen;

        return $this;
    }

    public function getWdeFctSecGen(): ?string
    {
        return $this->wdeFctSecGen;
    }

    public function setWdeFctSecGen(?string $wdeFctSecGen): self
    {
        $this->wdeFctSecGen = $wdeFctSecGen;

        return $this;
    }

    public function getWdeMelSecGen(): ?string
    {
        return $this->wdeMelSecGen;
    }

    public function setWdeMelSecGen(?string $wdeMelSecGen): self
    {
        $this->wdeMelSecGen = $wdeMelSecGen;

        return $this;
    }

    public function getWdeIntituleDecl(): ?string
    {
        return $this->wdeIntituleDecl;
    }

    public function setWdeIntituleDecl(?string $wdeIntituleDecl): self
    {
        $this->wdeIntituleDecl = $wdeIntituleDecl;

        return $this;
    }

    public function getWdeAnneeDecl(): ?string
    {
        return $this->wdeAnneeDecl;
    }

    public function setWdeAnneeDecl(string $wdeAnneeDecl): self
    {
        $this->wdeAnneeDecl = $wdeAnneeDecl;

        return $this;
    }

    public function getWdeTranche08(): ?int
    {
        return $this->wdeTranche08;
    }

    public function setWdeTranche08(?int $wdeTranche08): self
    {
        $this->wdeTranche08 = $wdeTranche08;

        return $this;
    }

    public function getWdeTranche09(): ?int
    {
        return $this->wdeTranche09;
    }

    public function setWdeTranche09(?int $wdeTranche09): self
    {
        $this->wdeTranche09 = $wdeTranche09;

        return $this;
    }

    public function getWdeTranche10(): ?int
    {
        return $this->wdeTranche10;
    }

    public function setWdeTranche10(?int $wdeTranche10): self
    {
        $this->wdeTranche10 = $wdeTranche10;

        return $this;
    }

    public function getWdeTranche11(): ?int
    {
        return $this->wdeTranche11;
    }

    public function setWdeTranche11(?int $wdeTranche11): self
    {
        $this->wdeTranche11 = $wdeTranche11;

        return $this;
    }

    public function getWdeTranche12(): ?int
    {
        return $this->wdeTranche12;
    }

    public function setWdeTranche12(?int $wdeTranche12): self
    {
        $this->wdeTranche12 = $wdeTranche12;

        return $this;
    }

    public function getWdeTranche13(): ?int
    {
        return $this->wdeTranche13;
    }

    public function setWdeTranche13(?int $wdeTranche13): self
    {
        $this->wdeTranche13 = $wdeTranche13;

        return $this;
    }

    public function getWdeTranche14(): ?int
    {
        return $this->wdeTranche14;
    }

    public function setWdeTranche14(?int $wdeTranche14): self
    {
        $this->wdeTranche14 = $wdeTranche14;

        return $this;
    }

    public function getWdeTranche15(): ?int
    {
        return $this->wdeTranche15;
    }

    public function setWdeTranche15(?int $wdeTranche15): self
    {
        $this->wdeTranche15 = $wdeTranche15;

        return $this;
    }

    public function getWdeTranche16(): ?int
    {
        return $this->wdeTranche16;
    }

    public function setWdeTranche16(?int $wdeTranche16): self
    {
        $this->wdeTranche16 = $wdeTranche16;

        return $this;
    }

    public function getWdeTranche17(): ?int
    {
        return $this->wdeTranche17;
    }

    public function setWdeTranche17(?int $wdeTranche17): self
    {
        $this->wdeTranche17 = $wdeTranche17;

        return $this;
    }

    public function getWdeTranche18(): ?int
    {
        return $this->wdeTranche18;
    }

    public function setWdeTranche18(?int $wdeTranche18): self
    {
        $this->wdeTranche18 = $wdeTranche18;

        return $this;
    }

    public function getWdeTranche19(): ?int
    {
        return $this->wdeTranche19;
    }

    public function setWdeTranche19(?int $wdeTranche19): self
    {
        $this->wdeTranche19 = $wdeTranche19;

        return $this;
    }

    public function getWdeTranche20(): ?int
    {
        return $this->wdeTranche20;
    }

    public function setWdeTranche20(?int $wdeTranche20): self
    {
        $this->wdeTranche20 = $wdeTranche20;

        return $this;
    }

    public function getWdeTranche21(): ?int
    {
        return $this->wdeTranche21;
    }

    public function setWdeTranche21(?int $wdeTranche21): self
    {
        $this->wdeTranche21 = $wdeTranche21;

        return $this;
    }

    public function getWdeTranche22(): ?int
    {
        return $this->wdeTranche22;
    }

    public function setWdeTranche22(?int $wdeTranche22): self
    {
        $this->wdeTranche22 = $wdeTranche22;

        return $this;
    }

    public function getWdeIdCtRespQuestion(): ?int
    {
        return $this->wdeIdCtRespQuestion;
    }

    public function setWdeIdCtRespQuestion(?int $wdeIdCtRespQuestion): self
    {
        $this->wdeIdCtRespQuestion = $wdeIdCtRespQuestion;

        return $this;
    }

    public function getWdeIdCtResp(): ?int
    {
        return $this->wdeIdCtResp;
    }

    public function setWdeIdCtResp(?int $wdeIdCtResp): self
    {
        $this->wdeIdCtResp = $wdeIdCtResp;

        return $this;
    }

    public function getWdeTelResp(): ?string
    {
        return $this->wdeTelResp;
    }

    public function setWdeTelResp(?string $wdeTelResp): self
    {
        $this->wdeTelResp = $wdeTelResp;

        return $this;
    }

    public function getWdeFaxResp(): ?string
    {
        return $this->wdeFaxResp;
    }

    public function setWdeFaxResp(?string $wdeFaxResp): self
    {
        $this->wdeFaxResp = $wdeFaxResp;

        return $this;
    }

    public function getWdeEffectif2(): ?int
    {
        return $this->wdeEffectif2;
    }

    public function setWdeEffectif2(?int $wdeEffectif2): self
    {
        $this->wdeEffectif2 = $wdeEffectif2;

        return $this;
    }

    public function getWdeEffectif3(): ?int
    {
        return $this->wdeEffectif3;
    }

    public function setWdeEffectif3(?int $wdeEffectif3): self
    {
        $this->wdeEffectif3 = $wdeEffectif3;

        return $this;
    }

    public function getWdeEffectif4(): ?int
    {
        return $this->wdeEffectif4;
    }

    public function setWdeEffectif4(?int $wdeEffectif4): self
    {
        $this->wdeEffectif4 = $wdeEffectif4;

        return $this;
    }

    public function getWdeIsChorusPro(): ?int
    {
        return $this->wdeIsChorusPro;
    }

    public function setWdeIsChorusPro(int $wdeIsChorusPro): self
    {
        $this->wdeIsChorusPro = $wdeIsChorusPro;

        return $this;
    }

    public function getWdeSiret(): ?string
    {
        return $this->wdeSiret;
    }

    public function setWdeSiret(string $wdeSiret): self
    {
        $this->wdeSiret = $wdeSiret;

        return $this;
    }

    public function getWdeServiceCode(): ?string
    {
        return $this->wdeServiceCode;
    }

    public function setWdeServiceCode(string $wdeServiceCode): self
    {
        $this->wdeServiceCode = $wdeServiceCode;

        return $this;
    }

    public function getWdeIsOrderNumber(): ?int
    {
        return $this->wdeIsOrderNumber;
    }

    public function setWdeIsOrderNumber(?int $wdeIsOrderNumber): self
    {
        $this->wdeIsOrderNumber = $wdeIsOrderNumber;

        return $this;
    }

    public function getWdeOrderNumber(): ?string
    {
        return $this->wdeOrderNumber;
    }

    public function setWdeOrderNumber(string $wdeOrderNumber): self
    {
        $this->wdeOrderNumber = $wdeOrderNumber;

        return $this;
    }

    public function getWdeIdCtFactQuestion(): ?int
    {
        return $this->wdeIdCtFactQuestion;
    }

    public function setWdeIdCtFactQuestion(?int $wdeIdCtFactQuestion): self
    {
        $this->wdeIdCtFactQuestion = $wdeIdCtFactQuestion;

        return $this;
    }

    public function getWdeIdCtFact(): ?int
    {
        return $this->wdeIdCtFact;
    }

    public function setWdeIdCtFact(?int $wdeIdCtFact): self
    {
        $this->wdeIdCtFact = $wdeIdCtFact;

        return $this;
    }

    public function getWdeNomFact(): ?string
    {
        return $this->wdeNomFact;
    }

    public function setWdeNomFact(?string $wdeNomFact): self
    {
        $this->wdeNomFact = $wdeNomFact;

        return $this;
    }

    public function getWdeTitFact(): ?string
    {
        return $this->wdeTitFact;
    }

    public function setWdeTitFact(?string $wdeTitFact): self
    {
        $this->wdeTitFact = $wdeTitFact;

        return $this;
    }

    public function getWdeFctFact(): ?string
    {
        return $this->wdeFctFact;
    }

    public function setWdeFctFact(?string $wdeFctFact): self
    {
        $this->wdeFctFact = $wdeFctFact;

        return $this;
    }

    public function getWdeRaisoc1Fact(): ?string
    {
        return $this->wdeRaisoc1Fact;
    }

    public function setWdeRaisoc1Fact(?string $wdeRaisoc1Fact): self
    {
        $this->wdeRaisoc1Fact = $wdeRaisoc1Fact;

        return $this;
    }

    public function getWdeAdresse1Fact(): ?string
    {
        return $this->wdeAdresse1Fact;
    }

    public function setWdeAdresse1Fact(?string $wdeAdresse1Fact): self
    {
        $this->wdeAdresse1Fact = $wdeAdresse1Fact;

        return $this;
    }

    public function getWdeAdresse2Fact(): ?string
    {
        return $this->wdeAdresse2Fact;
    }

    public function setWdeAdresse2Fact(?string $wdeAdresse2Fact): self
    {
        $this->wdeAdresse2Fact = $wdeAdresse2Fact;

        return $this;
    }

    public function getWdeAdresse3Fact(): ?string
    {
        return $this->wdeAdresse3Fact;
    }

    public function setWdeAdresse3Fact(?string $wdeAdresse3Fact): self
    {
        $this->wdeAdresse3Fact = $wdeAdresse3Fact;

        return $this;
    }

    public function getWdeCoposFact(): ?string
    {
        return $this->wdeCoposFact;
    }

    public function setWdeCoposFact(?string $wdeCoposFact): self
    {
        $this->wdeCoposFact = $wdeCoposFact;

        return $this;
    }

    public function getWdeVilleFact(): ?string
    {
        return $this->wdeVilleFact;
    }

    public function setWdeVilleFact(?string $wdeVilleFact): self
    {
        $this->wdeVilleFact = $wdeVilleFact;

        return $this;
    }

    public function getWdeTelFact(): ?string
    {
        return $this->wdeTelFact;
    }

    public function setWdeTelFact(?string $wdeTelFact): self
    {
        $this->wdeTelFact = $wdeTelFact;

        return $this;
    }

    public function getWdeMailFact(): ?string
    {
        return $this->wdeMailFact;
    }

    public function setWdeMailFact(?string $wdeMailFact): self
    {
        $this->wdeMailFact = $wdeMailFact;

        return $this;
    }

    public function getWdeIdAdrFactQuestion(): ?int
    {
        return $this->wdeIdAdrFactQuestion;
    }

    public function setWdeIdAdrFactQuestion(?int $wdeIdAdrFactQuestion): self
    {
        $this->wdeIdAdrFactQuestion = $wdeIdAdrFactQuestion;

        return $this;
    }

    public function getWdeIdAdrFact(): ?int
    {
        return $this->wdeIdAdrFact;
    }

    public function setWdeIdAdrFact(?int $wdeIdAdrFact): self
    {
        $this->wdeIdAdrFact = $wdeIdAdrFact;

        return $this;
    }

    public function getRoles()
    {
//        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function getPassword()
    {
        return $this->wdePassword;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getUsername()
    {
        return $this->wdeDossier;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getMappedTokens(AuthorizedApp $app)
    {
        $tokens = $this->tokens->toArray();

        $out = [];
        /** @var Token $token */
        foreach ($tokens as $token) {
            if ( ! empty($token->getApp())) {
                if ($token->getApp()->getId() == $app->getId()) {
                    $out[$token->getType()] = [
                        'hash'       => $token->getHash(),
                        'expires_at' => $token->getExpiresAt()->getTimestamp()
                    ];
                }
            }
        }

        if (count($out) == 0) {
            return false;
        }

        return $out;
    }

    public function makeInstanceForAuthenticator()
    {
        $this->instanceForAuthenticator = true;
    }

    public function getSSOData()
    {
        return [
            'username'                 => $this->getUsername(),
            'wdePassword'              => $this->getWdePassword(),
            'wdeTypcont'               => $this->getWdeTypcont(),
            'wdeStamp'                 => $this->getWdeStamp(),
            'wdeSynchro'               => $this->getWdeSynchro(),
            'wdeContrat'               => $this->getWdeContrat(),
            'wdeAnnee'                 => $this->getWdeAnnee(),
            'wdeNumperiod'             => $this->getWdeNumperiod(),
            'wdeEleves'                => $this->getWdeEleves(),
            'wdeTranche'               => $this->getWdeTranche(),
            'wdeCopieurs'              => $this->getWdeCopieurs(),
            'wdeProfs'                 => $this->getWdeProfs(),
            'wdeEtatDeclar'            => $this->getWdeEtatDeclar(),
            'wdeDateValidation'        => $this->getWdeDateValidation(),
            'wdeEmailSent'             => $this->getWdeEmailSent(),
            'wdeNomResp'               => $this->getWdeNomResp(),
            'wdeTitResp'               => $this->getWdeTitResp(),
            'wdeFctResp'               => $this->getWdeFctResp(),
            'wdeMelResp'               => $this->getWdeMelResp(),
            'wdeEffectif'              => $this->getWdeEffectif(),
            'wdeTranche01'             => $this->getWdeTranche01(),
            'wdeTranche02'             => $this->getWdeTranche02(),
            'wdeTranche03'             => $this->getWdeTranche03(),
            'wdeTranche04'             => $this->getWdeTranche04(),
            'wdeTranche05'             => $this->getWdeTranche05(),
            'wdeTranche06'             => $this->getWdeTranche06(),
            'wdeTranche07'             => $this->getWdeTranche07(),
            'wdeIdCtNomSecGenQuestion' => $this->getWdeIdCtNomSecGenQuestion(),
            'wdeIdCtNomSecGen'         => $this->getWdeIdCtNomSecGen(),
            'wdeNomSecGen'             => $this->getWdeNomSecGen(),
            'wdeTitSecGen'             => $this->getWdeTitSecGen(),
            'wdeFctSecGen'             => $this->getWdeFctSecGen(),
            'wdeMelSecGen'             => $this->getWdeMelSecGen(),
            'wdeIntituleDecl'          => $this->getWdeIntituleDecl(),
            'wdeAnneeDecl'             => $this->getWdeAnneeDecl(),
            'wdeTranche08'             => $this->getWdeTranche08(),
            'wdeTranche09'             => $this->getWdeTranche09(),
            'wdeTranche10'             => $this->getWdeTranche10(),
            'wdeTranche11'             => $this->getWdeTranche11(),
            'wdeTranche12'             => $this->getWdeTranche12(),
            'wdeTranche13'             => $this->getWdeTranche13(),
            'wdeTranche14'             => $this->getWdeTranche14(),
            'wdeTranche15'             => $this->getWdeTranche15(),
            'wdeTranche16'             => $this->getWdeTranche16(),
            'wdeTranche17'             => $this->getWdeTranche17(),
            'wdeTranche18'             => $this->getWdeTranche18(),
            'wdeTranche19'             => $this->getWdeTranche19(),
            'wdeTranche21'             => $this->getWdeTranche21(),
            'wdeTranche22'             => $this->getWdeTranche22(),
            'wdeIdCtRespQuestion'      => $this->getWdeIdCtRespQuestion(),
            'wdeIdCtResp'              => $this->getWdeIdCtResp(),
            'wdeTelResp'               => $this->getWdeTelResp(),
            'wdeFaxResp'               => $this->getWdeFaxResp(),
            'wdeEffectif2'             => $this->getWdeEffectif2(),
            'wdeEffectif3'             => $this->getWdeEffectif3(),
            'wdeEffectif4'             => $this->getWdeEffectif4(),
            'wdeIsChorusPro'           => $this->getWdeIsChorusPro(),
            'wdeSiret'                 => $this->getWdeSiret(),
            'wdeServiceCode'           => $this->getWdeServiceCode(),
            'wdeIsOrderNumber'         => $this->getWdeIsOrderNumber(),
            'wdeOrderNumber'           => $this->getWdeOrderNumber(),
            'wdeIdCtFactQuestion'      => $this->getWdeIdCtFactQuestion(),
            'wdeIdCtFact'              => $this->getWdeIdCtFact(),
            'wdeNomFact'               => $this->getWdeNomFact(),
            'wdeTitFact'               => $this->getWdeTitFact(),
            'wdeFctFact'               => $this->getWdeFctFact(),
            'wdeRaisoc1Fact'           => $this->getWdeRaisoc1Fact(),
            'wdeAdresse1Fact'          => $this->getWdeAdresse1Fact(),
            'wdeAdresse2Fact'          => $this->getWdeAdresse2Fact(),
            'wdeAdresse3Fact'          => $this->getWdeAdresse3Fact(),
            'wdeCoposFact'             => $this->getWdeCoposFact(),
            'wdeVilleFact'             => $this->getWdeVilleFact(),
            'wdeTelFact'               => $this->getWdeTelFact(),
            'wdeMailFact'              => $this->getWdeMailFact(),
            'wdeIdAdrFactQuestion'     => $this->getWdeIdAdrFactQuestion(),
            'wdeIdAdrFact'             => $this->getWdeIdAdrFact(),
            'roles'                    => $this->getRoles(true),
        ];
    }
}
