<?php

namespace App\Entity;

use App\Repository\AuthorizedAppRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="security_authorized_apps")
 * @ORM\Entity(repositoryClass=AuthorizedAppRepository::class)
 */
class AuthorizedApp
{
    public const APP_TYPE_WEB = "web";
    public const APP_TYPE_MOBILE = "mobile";
    public const APP_TYPE_SERVICE = "service";

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $returnURL;

    /**
     * @ORM\Column(name="`key`", type="string", length=255, unique=true)
     */
    private $key;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    private $secret;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAllowedAdmin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Token", mappedBy="app")
     */
    private $tokens;

    public function __construct()
    {
        $this->key = null;
        $this->tokens = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return AuthorizedApp
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return AuthorizedApp
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getReturnURL()
    {
        return $this->returnURL;
    }

    /**
     * @param mixed $returnURL
     * @return AuthorizedApp
     */
    public function setReturnURL($returnURL)
    {
        $this->returnURL = $returnURL;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return AuthorizedApp
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param mixed $secret
     * @return AuthorizedApp
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAllowedAdmin()
    {
        return $this->isAllowedAdmin;
    }

    /**
     * @param mixed $isAllowedAdmin
     * @return AuthorizedApp
     */
    public function setIsAllowedAdmin($isAllowedAdmin)
    {
        $this->isAllowedAdmin = $isAllowedAdmin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @param mixed $tokens
     * @return AuthorizedApp
     */
    public function setTokens($tokens)
    {
        $this->tokens = $tokens;
        return $this;
    }

    public function __toString()
    {
        return $this->name.'';
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return ($this->getIsAllowedAdmin()) ? ['ROLE_APP_ADMIN'] : null;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->getSecret();
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getKey();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
