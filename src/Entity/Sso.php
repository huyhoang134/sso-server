<?php

namespace App\Entity;

use App\Entity\AuthorizedApp;
use App\Entity\Token;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Sso
 *
 * @ORM\Table(name="SSO")
 * @ORM\Entity
 */
class Sso
{
    /**
     * @var string
     *
     * @ORM\Column(name="sso_login", type="string", length=255, nullable=false)
     */
    private $ssoLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="sso_password", type="string", length=255, nullable=false)
     */
    private $ssoPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="sso_table", type="string", length=255, nullable=false)
     */
    private $ssoTable;

    /**
     * @var int
     *
     * @ORM\Column(name="sso_id_reccord", type="integer", nullable=false)
     */
    private $ssoIdReccord;

    public function getSsoLogin(): ?string
    {
        return $this->ssoLogin;
    }

    public function getSsoPassword(): ?string
    {
        return $this->ssoPassword;
    }

    public function setSsoPassword(string $ssoPassword): self
    {
        $this->ssoPassword = $ssoPassword;

        return $this;
    }

    public function getSsoTable(): ?string
    {
        return $this->ssoTable;
    }

    public function setSsoTable(string $ssoTable): self
    {
        $this->ssoTable = $ssoTable;

        return $this;
    }

    public function getSsoIdReccord(): ?int
    {
        return $this->ssoIdReccord;
    }

    public function setSsoIdReccord(int $ssoIdReccord): self
    {
        $this->ssoIdReccord = $ssoIdReccord;

        return $this;
    }

    public function getMappedTokens(AuthorizedApp $app)
    {
        $tokens = $this->tokens->toArray();
    
        $out = [];
        /** @var Token $token */
        foreach ($tokens as $token) {
            if ( ! empty($token->getApp())) {
                if ($token->getApp()->getId() == $app->getId()) {
                    $out[$token->getType()] = [
                        'hash'       => $token->getHash(),
                        'expires_at' => $token->getExpiresAt()->getTimestamp()
                    ];
                }
            }
        }
    
        if (count($out) == 0) {
            return false;
        }
    
        return $out;
    }

}
