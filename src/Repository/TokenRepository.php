<?php

namespace App\Repository;

use App\Entity\Wdeclar;
use App\Entity\Token;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Security\User\CfcUserProvider;


/**
 * @method Token|null find($id, $lockMode = null, $lockVersion = null)
 * @method Token|null findOneBy(array $criteria, array $orderBy = null)
 * @method Token[]    findAll()
 * @method Token[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TokenRepository extends ServiceEntityRepository
{
    /**
     * @var CfcUserProvider
     */
    private $cfcUserProvider;
    
    public function __construct(ManagerRegistry $registry, CfcUserProvider $cfcUserProvider)
    {
        $this->cfcUserProvider = $cfcUserProvider;
        
        parent::__construct($registry, Token::class);
    }

    public function createRefreshToken($userToken)
    {
        return $this->createToken($userToken, \App\Entity\Token::TOKEN_TYPE_REFRESH, 64, "P30D");
    }

    public function createAccessToken($userToken)
    {
        return $this->createToken($userToken, Token::TOKEN_TYPE_ACCESS, 32, "PT1H");
    }

    public function createVerifierToken($userToken)
    {
        return $this->createToken($userToken, Token::TOKEN_TYPE_VERIFIER, 128, "P30D");
    }

    public function createToken( $userToken, $type, $hashLength, $TTL)
    {
        $token = new Token();
        $token->setUserToken($userToken);
        $token->setType($type);
        $token->setCreatedAt(new \DateTime());
        $token->setLastUsedAt(new \DateTime());
        $token->setExpiresAt((new \DateTime())->add(new \DateInterval($TTL)));
        $token->setHash($this->randomizeCase(bin2hex(random_bytes($hashLength))));
        $this->_em->persist($token);
        $this->_em->flush();

        return $token;
    }

    private function randomizeCase($string)
    {
        $str = str_split($string);
        for ($i = 0; $i < strlen($string) ; $i++)
        {
            $str[$i] = rand(0, 100) > 50 ? strtoupper($str[$i]) : $str[$i];
        }

        return implode('', $str);
    }
    
    public function getUser($token)
    {
        return $this->cfcUserProvider->loadUserByUserToken($token);
 
    }
    
    
}
