<?php

namespace App\Services\Security;

use App\Entity\AuthorizedApp;
use App\Entity\Token;
use App\Entity\Wdeclar;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserManager implements UserLoaderInterface
{

    private $em;

    private $dispatcher;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }
    
    public function loadUserByUsername($username)
    {
        // TODO: Implement loadUserByUsername() method.
    }

    /**
     * @param Wdeclar $user
     * @param AuthorizedApp $app
     *
     * @return Wdeclar
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function refreshUserTokens($userToken, AuthorizedApp $app)
    {
        $accessToken = $this->em->getRepository(Token::class)->findOneBy([
            'userToken' => $userToken,
            'app' => $app,
            'type' => Token::TOKEN_TYPE_ACCESS
        ]);

        if ($accessToken !== null && $accessToken->getExpiresAt()->getTimestamp() < time()) {
            $this->em->remove($accessToken);
            $this->em->flush();
        }

        $app = $this->em->getRepository(AuthorizedApp::class)->find($app->getId());

        $tokens = $this->em->getRepository(Token::class)->findBy(['app' => $app, 'userToken' => $userToken]);
        if (count($tokens) < 3) {
            $existingTypes = [];
            foreach ($tokens as $token)
            {
                $existingTypes[] = $token->getType();
            }

            $newTokens = new ArrayCollection();
            if (!in_array(Token::TOKEN_TYPE_ACCESS, $existingTypes)) {
                $newTokens->add($this->em->getRepository(Token::class)->createAccessToken($userToken)->setApp($app));
            }
//            if (!in_array(Token::TOKEN_TYPE_REFRESH, $existingTypes)) {
//                $newTokens->add($this->em->getRepository(Token::class)->createRefreshToken($user)->setApp($app));
//            }
//            if (!in_array(Token::TOKEN_TYPE_VERIFIER, $existingTypes)) {
//                $newTokens->add($this->em->getRepository(Token::class)->createVerifierToken($user)->setApp($app));
//            }

            foreach ($newTokens as $token)
            {
                $this->em->persist($token);
                $this->em->flush();
            }
        }

        $tokens = $this->em->getRepository(Token::class)->findBy(['app' => $app, 'userToken' => $userToken]);
        foreach ($tokens as $token)
        {
            $token->setExpiresAt((new \DateTime())->add(new \DateInterval("P6M")));
            $token->setLastUsedAt(new \DateTime());
            $this->em->persist($token);
            $this->em->flush();
        }

        return $userToken;
    }

    /**
     * @param $token
     *
     * @return false|Wdeclar
     */
    public function loadUserByToken($token)
    {
        /** @var Token $token */
        $token = $this->em->getRepository(Token::class)->findOneBy([
            'hash' => $token,
            'type' => Token::TOKEN_TYPE_ACCESS
        ]);
        
        if (!$token) {
            return false;
        }
        
        return $token->getUserToken();
    }
}
