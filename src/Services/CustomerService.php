<?php

namespace App\Services;

use App\Entity\Token;
use App\Entity\Wdeclar;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CustomerService 
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManager;
        $this->params = $params;
    }
    
    public function getUserByToken($token)
    {
       
        /** @var Token $token */
        $tokenRepository = $this->entityManager->getRepository(Token::class);
        $token=$tokenRepository->findOneBy(['hash' => $token]);

        
        
        
        //from token grabbed from hash we can grabb all the conresponding lines from SSO by comparing md5 token (login.passord), and corresponding md5 token in SSO table.
        return !empty($token->getUserToken()) ?$tokenRepository->getUser($token->getUserToken()) : false;
    }

    /**
     * @param string $parameter
     *
     * @return false|string
     */
    public function encryptParameter(string $parameter)
    {
        // Store the cipher method
        $ciphering = "AES-256-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        // Non-NULL Initialization Vector for encryption
        $encryption_iv = $this->params->get('encryption_iv');

        // Store the encryption key
        $encryption_key = $this->params->get('encryption_key');

        // Use openssl_encrypt() function to encrypt the data
        return openssl_encrypt($parameter, $ciphering,
            $encryption_key, $options, $encryption_iv);
    }

    /**
     * @param string $parameter
     *
     * @return false|string
     */
    public function decryptParameter(string $parameter)
    {
        // Store the cipher method
        $ciphering = "AES-256-CBC";

        // Use OpenSSl Encryption method
        $options = 0;

        $decryption_iv = $this->params->get('encryption_iv');

        // Store the decryption key
        $decryption_key = $this->params->get('encryption_key');

        // Use openssl_decrypt() function to decrypt the data
        return openssl_decrypt ($parameter, $ciphering,
            $decryption_key, $options, $decryption_iv);
    }
}
