<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{
    /**
     * @Route("/api/articles", name="api_articles")
     *
     * return JsonResponse
     */
    public function articlesAction(): JsonResponse
    {
        $articles = array('article1', 'article2', 'article3');
        return new JsonResponse($articles);
    }
}
