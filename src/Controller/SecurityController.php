<?php

namespace App\Controller;

use App\Entity\AuthorizedApp;
use App\Entity\Wdeclar;
use App\Security\User\CfcUserProvider;
use App\Services\CustomerService;
use App\Services\Security\UserManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @var CfcUserProvider
     */
    private $cfcUserProvider;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var CustomerService
     */
    private $customerService;

    public function __construct(
        CfcUserProvider $cfcUserProvider,
        UserManager $userManager,
        UrlGeneratorInterface $urlGenerator,
        CustomerService $customerService
    ) {
        $this->cfcUserProvider = $cfcUserProvider;
        $this->userManager     = $userManager;
        $this->urlGenerator    = $urlGenerator;
        $this->customerService = $customerService;
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {

    }

    /**
     * @Route("/login-success", name="login_success")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function loginSuccessAction(Request $request)
    {
        $redirectURL = $request->getSession()->get('login_redirect', '/' . $request->getLocale() . '/');
        $request->getSession()->remove('login_redirect');
        if ($redirectURL == '') {
            $redirectURL = '/';
        }

        return $this->redirect($redirectURL);
    }

    /**
     * @Route("/api/v1/security/login-grant", name="login_grant", methods={"GET","POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function loginGrantAction(Request $request)
    {
        /*$obj=$this->cfcUserProvider->loadUserByUsernamePassword("0010183U", "CEQISUQA");
        sm($obj);
        die();*/
        $username  = $request->get('username', null);
        $password  = $request->get('password', null);
        $appID     = $request->headers->get('X-APP-ID', null);
        $appSecret = $request->headers->get('X-APP-SECRET', null);
        
        /*
         * Test data **/
        /*$username  = 1601;
        $password  = "WCO_pwd";
        $appID     = "c5f5484cceff1f7a9c182721799a914a";
        $appSecret = "40882e14c19a0f87a70b157b58c684c0fa19609d56384c520da1660f005d0aa44635acf5e3b2d935208a5f6448a5cba8a101a4a896d5bb6113ce9acfa7043313";
        */
         
  

        /** @var AuthorizedApp $app */
        $app = $this->getDoctrine()
                    ->getRepository(AuthorizedApp::class)
                    ->findOneBy([
                        'key'    => $appID,
                        'secret' => $appSecret
                    ]);


        if ($appID === null || $app=== null) {
            $msg = 'This app is not allowed to access secure data!';

            return $this->json(['status' => 'error', 'message' => $msg]);
        }

        if ($username === null || $password === null) {
            $msg = 'Username and password are required for this grant!';

            return $this->json(['status' => 'error', 'message' => $msg]);
        }
        
        
        /*return $this->json(['status' => 'error', 'messagess' => serialize($obj) ]);
        sm($obj);
        die("status");
        return $this->json(['status' => 'error', 'message' => serialize($obj) ]);*/

        try {
            $user=$this->cfcUserProvider->loadUserByUsernamePassword($username, $password);
            //$user = $this->cfcUserProvider->loadUserByUsername($username);
        } catch (Exception $e) {
            $user = null;
        }


        if (empty($user) || $user === null) {
            $msg = 'Username or password don\'t match!';

            return $this->json(['status' => 'error', 'message' => $msg], 400);
        }

        /*if ($user->getPassword() !== $password) {
            $msg = 'Username or password don\'t match!';

            return $this->json(['status' => 'error', 'message' => $msg], 400);
        }*/

        /** @var Wdeclar $user */
        $userToken = $this->userManager->refreshUserTokens(md5($username.$password), $app);

        $tokens = $this->cfcUserProvider->getMappedTokens(md5($username.$password),$app); //$user->getMappedTokens($app);


        //
        $response = new Response();
        $cookie   = new Cookie('SaveToken', $this->customerService->encryptParameter($tokens['access_token']['hash']), time() + 3600);
        $response->headers->setCookie($cookie);
        $response->send();

        $data = [
            'app'    => $app->getName(),
            'tokens' => $tokens,
            //'user'   => $userToken,
            'user'   => $user,
        ];

        return $this->json(['status' => 'ok', 'data' => $data], 200);
    }

    /**
     * @Route("/security/cache-check", name="cache_check", methods={"GET"})
     *
     * @param Request $request
     *
     */
    public function cacheCheckAction(Request $request)
    {
        $appKey      = $request->get('appKey', null);
        $appSecret   = $request->get('appSecret', null);
        $username    = $request->get('username', null);
        $urlBackLink = $request->get('urlBackLink', null);

        if ( ! $appKey || ! $appSecret || ! $username || ! $urlBackLink) {
            //TODO return error page
            return new RedirectResponse($this->urlGenerator->generate('index_page'), Response::HTTP_FORBIDDEN);
        }

        $appKeyDecrypt      = $this->customerService->decryptParameter($appKey);
        $appSecretDecrypt   = $this->customerService->decryptParameter($appSecret);
        $usernameDecrypt    = $this->customerService->decryptParameter($username);
        $urlBackLinkDecrypt = $this->customerService->decryptParameter($urlBackLink);

        /** @var AuthorizedApp $app */
        $app = $this->getDoctrine()
                    ->getRepository(AuthorizedApp::class)
                    ->findOneBy([
                        'key'    => $appKeyDecrypt,
                        'secret' => $appSecretDecrypt
                    ]);

        if ($appKeyDecrypt === null || $app === null) {
            $msg = 'This app is not allowed to access secure data!';

            return $this->json(['status' => 'error', 'message' => $msg]);
        }

        if ($usernameDecrypt === null) {
            $msg = 'Username are required for this grant!';

            return $this->json(['status' => 'error', 'message' => $msg]);
        }

        
        //$user = $this->cfcUserProvider->loadUserByUserToken($usernameDecrypt);
        
        /** @var Wdeclar $user */
        //$userdata = $this->userManager->refreshUserTokens($usernameDecrypt, $app);

        
        $tokens = $this->cfcUserProvider->getMappedTokens($usernameDecrypt, $app);

        /*$response = new Response();
        $cookie   = new Cookie('SaveToken', $this->customerService->encryptParameter($tokens['access_token']['hash']), time() + 3600);
        $response->headers->setCookie($cookie);
        $response->send();*/

        setcookie("SaveToken",$this->customerService->encryptParameter($tokens['access_token']['hash']), time() + 3600, '/');
        header('Location:  '.$urlBackLinkDecrypt . '?' . 'logoutChecked=true');
        exit();

        //return new RedirectResponse($urlBackLinkDecrypt);
    }

    /**
     * @Route("/api/v1/security/get-user-by-token", name="get_user_by_token", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     */
    public function getUserByTokenAction(Request $request): JsonResponse
    {
        $appKey     = $request->headers->get("x-app-id");//$request->get('appKey', null);
        $appSecret  = $request->headers->get("x-app-secret");//$request->get('appSecret', null);
        $savedToken = $request->get('SaveToken', null);


        if ( ! $appKey || ! $appSecret || ! $savedToken) {
            return $this->json(['status' => 'ok', 'data' => 'Access denied'], 400);
        }

        $app = $this->getDoctrine()
                    ->getRepository(AuthorizedApp::class)
                    ->findOneBy([
                        'key'    => $appKey,
                        'secret' => $appSecret
                    ]);

        if ($appKey === null || $app === null) {
            $msg = 'This app is not allowed to access secure data!';

            return $this->json(['status' => 'error', 'message' => $msg]);
        }

        $user = $this->customerService->getUserByToken($this->customerService->decryptParameter($savedToken));
        return $this->json(['status' => 'error', 'message' => $user]); ;

    }

    /**
     * @Route("/security/cache-saved-check", name="cache_saved_check", methods={"GET"})
     *
     * @param Request $request
     *
     */
    public function cacheSavedCheckAction(Request $request)
    {
       
        $appKey      = $request->get('appKey', null);
        $appSecret   = $request->get('appSecret', null);
        $urlBackLink = $request->get('urlBackLink', null);

        if ( ! $appKey || ! $appSecret || ! $urlBackLink) {
            header('Location:  '.$this->urlGenerator->generate('index_page'));
            exit();
        }

        $appKeyDecrypt      = $this->customerService->decryptParameter($appKey);
        $appSecretDecrypt   = $this->customerService->decryptParameter($appSecret);
        $urlBackLinkDecrypt = $this->customerService->decryptParameter($urlBackLink);
        $savedToken         = $request->cookies->get('SaveToken');

        $app = $this->getDoctrine()
                    ->getRepository(AuthorizedApp::class)
                    ->findOneBy([
                        'key'    => $appKeyDecrypt,
                        'secret' => $appSecretDecrypt
                    ]);

        if ($appKeyDecrypt === null || $app === null) {
            header('Location:  '.$urlBackLinkDecrypt . '?' . 'cookieChecked=true');
            exit();
        }

        if ( ! $savedToken) {
            header('Location:  '.$urlBackLinkDecrypt . '?' . 'cookieLogoutChecked=true' . '&cookieChecked=true&msg=savedToken-is-null');
            exit();
        }

        $user = $this->customerService->getUserByToken($this->customerService->decryptParameter($savedToken));

        if ( ! $user) {
            header('Location:  '.$urlBackLinkDecrypt . '?' . 'cookieChecked=true');
            exit();
        }

        $username = 'backToken=' . urlencode($savedToken);
        header('Location:  '.$urlBackLinkDecrypt . '?' . $username . '&cookieChecked=true');
        exit();

    }

    /**
     * @Route("/security/remove-cache", name="remove_cache", methods={"GET"})
     *
     * @param Request $request
     *
     */
    public function removeCache(Request $request)
    {
        $urlBackLink = $request->get('urlBackLink', null);

        if ( ! $urlBackLink) {
            header('Location:  '.$this->urlGenerator->generate('index_page'));
            exit();
        }

        $urlBackLinkDecrypt = $this->customerService->decryptParameter($urlBackLink);

        setcookie("SaveToken","", time()-3600, '/');
        header('Location:  '.$urlBackLinkDecrypt . '?' . 'logoutChecked=true');
        exit();

    }
}
