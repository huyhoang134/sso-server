<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index_page")
     */
    public function index()
    {
        $auth_checker = $this->get('security.authorization_checker');

        $token = $this->get('security.token_storage')->getToken();
        $user = $token->getUser();
        
        $isRoleUser = $auth_checker->isGranted('ROLE_USER');

        return $this->render('index.html.twig', [
            '$user' => $user
        ]);
    }
}
